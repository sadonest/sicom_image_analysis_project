"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np

from src.forward_model import CFA
from src.methods.antoine_vouillon.functions import *

#!!!!!!!! It is normal that the reconstructions lasts several minutes (3min on my computer)

def run_reconstruction(y: np.ndarray, cfa: str) -> np.ndarray:
    """Performs demosaicking on y.

    Args:
        y (np.ndarray): Mosaicked image to be reconstructed.
        cfa (str): Name of the CFA. Can be bayer or quad_bayer.

    Returns:
        np.ndarray: Demosaicked image.
    """

    # Define constants and operators
    cfa_name = 'bayer' # bayer or quad_bayer
    input_shape = (y.shape[0], y.shape[1], 3)
    op = CFA(cfa_name, input_shape)

    img_res = op.adjoint(y)
    N = img_res[:,:,0].shape[0]
    M = img_res[:,:,0].shape[1]

    #interpolating green channel

    for i in range (N):
        for j in range (M):
            if img_res[i,j,1] ==0:

                neighbors = find_neighbors(img_res,1,i,j,N,M)
                dir_deriv = find_dir_deriv(neighbors)
                weights = find_weights(img_res, neighbors, dir_deriv,1,i,j,N,M)

                img_res[i,j,1] = interpolate(neighbors,weights)
                
    
    img_res[img_res>1] = 1
    img_res[img_res<0] = 0

    #first intepolation of red channel

    for i in range (1,N,2):
        for j in range (0,M,2):
            
            neighbors = find_neighbors(img_res,0,i,j,N,M)

            neighbors_G = find_neighbors(img_res,1,i,j,N,M)
            dir_deriv = find_dir_deriv(neighbors_G)
            weights = find_weights(img_res,neighbors_G, dir_deriv,1,i,j,N,M) 



            img_res[i,j,0] = interpolate_RB(neighbors, neighbors_G, weights)

    # second interpolation of red channel

    for i in range (N):
        for j in range (M):
            if img_res[i,j,0] ==0:

                neighbors = find_neighbors(img_res,0,i,j,N,M)
                dir_deriv = find_dir_deriv(neighbors)
                weights = find_weights(img_res,neighbors, dir_deriv,0,i,j,N,M)

                img_res[i,j,0] = interpolate(neighbors,weights)

    img_res[img_res>1] = 1
    img_res[img_res<0] = 0

    #first interpolation of blue channel

    for i in range (0,N,2):
        for j in range (1,M,2):
            
            neighbors = find_neighbors(img_res,2,i,j,N,M)

            neighbors_G = find_neighbors(img_res,1,i,j,N,M)
            dir_deriv = find_dir_deriv(neighbors_G)
            weights = find_weights(img_res,neighbors_G, dir_deriv,1,i,j,N,M) 



            img_res[i,j,2] = interpolate_RB(neighbors, neighbors_G, weights)

    #second interpolation of blue channel

    for i in range (N):
        for j in range (M):
            if img_res[i,j,2] ==0:

                neighbors = find_neighbors(img_res,2,i,j,N,M)
                dir_deriv = find_dir_deriv(neighbors)
                weights = find_weights(img_res, neighbors, dir_deriv,2,i,j,N,M)

                img_res[i,j,2] = interpolate(neighbors,weights)

    img_res[img_res>1] = 1
    img_res[img_res<0] = 0

    return img_res


####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
