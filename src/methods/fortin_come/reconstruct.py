"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np

from src.forward_model import CFA
from src.methods.fortin_come.DemosaickingInterChannel import local_int


def run_reconstruction(y: np.ndarray, cfa: str) -> np.ndarray:
    """Performs demosaicking on y.

    Args:
        y (np.ndarray): Mosaicked image to be reconstructed.
        cfa (str): Name of the CFA. Can be bayer or quad_bayer.

    Returns:
        np.ndarray: Demosaicked image.
    """
    # Performing the reconstruction.

    input_shape = (y.shape[0], y.shape[1], 3)
    op = CFA(cfa, input_shape)
    z = op.adjoint(y)
    
    R0 = z[:, :, 0]
    G0 = z[:, :, 1]
    B0 = z[:, :, 2]
    
    beta = 0.9 # Inter-channel parameter
    L = 5 # Size of the local neighborhood
    eps = 0.00000001 # Tresholding parameter > 0
    
    R1, G1, B1 = local_int(R0, G0, B0, beta, L, eps)
    
    img_demosaicked = np.zeros((R1.shape[0], R1.shape[1], 3))
    img_demosaicked[:, :, 0] = R1
    img_demosaicked[:, :, 1] = G1
    img_demosaicked[:, :, 2] = B1
    

    return img_demosaicked

####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: FORTIN Côme
