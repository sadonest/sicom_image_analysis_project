import numpy as np

from scipy.signal import convolve2d


def down_sample(image):    

    down_sampled = np.empty((int(image.shape[0] / 2), int(image.shape[1] / 2)))

    down_sampled[:, :] = (image[::2, ::2] + image[1::2, ::2] + image[::2, 1::2] + image[1::2, 1::2]) / 4

    return down_sampled


def up_sample(image):

    up_sampled = np.empty((int(image.shape[0] * 2), int(image.shape[1] * 2), image.shape[2]))

    up_sampled[::2, ::2, :] = image[::1, ::1, :]
    up_sampled[1::2, ::2, :] = image[::1, ::1, :]
    up_sampled[::2, 1::2, :] = image[::1, ::1, :]
    up_sampled[1::2, 1::2, :] = image[::1, ::1, :]

    return up_sampled



def refine(image):
    
    res = np.empty((image.shape[0], image.shape[1], 3))
    
    ker = np.array([[1, 2, 1], [2, 4, 2], [1, 2, 1]]) / 16
    for i in range(3):
        res[:, :, i] = convolve2d(image[:, :, i], ker, mode='same')

    return res
