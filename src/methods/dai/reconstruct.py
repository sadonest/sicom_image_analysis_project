"""The main file for the baseline reconstruction.
This file should NOT be modified.
"""


import numpy as np

from src.forward_model import CFA
from src.methods.dai.function import Spectral_difference, quad_bayer_to_bayer


def run_reconstruction(y: np.ndarray, cfa: str) -> np.ndarray:
    """Performs demosaicking on y.

    Args:
        y (np.ndarray): Mosaicked image to be reconstructed.
        cfa (str): Name of the CFA. Can be bayer or quad_bayer.

    Returns:
        np.ndarray: Demosaicked image.
    """
    input_shape = (y.shape[0], y.shape[1], 3)
    op = CFA(cfa, input_shape)

    if op.cfa == 'bayer':
        res = Spectral_difference(op, y)

    else: 
        op.mask = quad_bayer_to_bayer(op.mask)
        res = Spectral_difference(CFA('bayer',input_shape),quad_bayer_to_bayer(y))

    return res


####a
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
